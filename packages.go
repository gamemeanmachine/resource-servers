package resources

import (
	"fmt"
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// Resource packages contain a lot of spaces, which are
// also loaded in the order they are registered. They,
// however, have an additional notion of "dependencies".
// This means: packages can depend on other packages to
// be registered first, or fail to do so. Aside from that,
// they register their spaces in a similar way the spaces
// register their resources lists.
type Package struct {
	key          string
	caption      string
	dependencies map[*Package]bool
	spacesMap    map[string]*Space
	spacesArray  []*Space
}


// Gets the package's key.
func (packg *Package) Key() string {
	return packg.key
}


// Gets the package's caption.
func (packg *Package) Caption() string {
	return packg.caption
}


// Enumerates all the dependencies required by
// this package, in the order they were required.
func (packg *Package) EnumerateDependencies(callback func(key string, dep *Package)) {
	for dep, _ := range packg.dependencies {
		callback(dep.Key(), dep)
	}
}


// Enumerates all the resources spaces registered
// in this package, in the order they were added.
func (packg *Package) EnumerateSpaces(callback func(key string, space *Space)) {
	for _, space := range packg.spacesArray {
		callback(space.Key(), space)
	}
}


// Gets a registered resources space by its key.
func (packg *Package) Get(key string) *Space {
	return packg.spacesMap[key]
}


// Adds a new list to this space, if the key is
// not already being used.
func (packg *Package) Register(space *Space) {
	if space == nil {
		panic(errors.Argument(
			"resources.Package::Register", "space",
			"Must not be nil",
		))
	} else if _, ok := packg.spacesMap[space.Key()]; ok {
		panic(errors.Argument(
			"resources.Space::Register", "space",
			fmt.Sprintf("A space with the same key already exists: %s", space.Key()),
		))
	} else {
		packg.spacesMap[space.Key()] = space
		packg.spacesArray = append(packg.spacesArray, space)
	}
}


// Adds a new dependency to this space. It does
// nothing if the dependency has already been added.
func (packg *Package) Require(dep *Package) {
	if dep == nil {
		panic(errors.Argument(
			"resources.Package::Require", "dep",
			"Must not be nil",
		))
	} else if dep == packg {
		panic(errors.Argument(
			"resources.Package::Require", "dep",
			"Must not be the same package",
		))
	} else {
		packg.dependencies[dep] = true
	}
}


// Creates a new resources space.
func NewPackage(key, caption string) *Package {
	if !keyPattern.MatchString(key) {
		panic(errors.Argument(
			"resources.NewSpace", "key",
			"Must consist of lowercase letters, numbers, and single-hyphens",
		))
	}
	return &Package{
		key,
		caption,
		map[*Package]bool{},
		map[string]*Space{},
		[]*Space{},
	}
}