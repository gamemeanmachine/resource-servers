package http


// Just an internal structure used for iteration
// of spaces, and lists.
type resultListEntry struct {
	Key, Caption string
}
