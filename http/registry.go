package http

import (
	"net/http"
	"encoding/json"
	"gitlab.com/gamemeanmachine/resource-servers"
)


// Returns an HTTP handler to just json-print the registered packages.
// This handler can be directly used by a standard HTTP server.
func RegistryAllHandler(registry *resources.Registry) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump := []resultListEntry{}
		registry.EnumeratePackages(func(key string, packg *resources.Package) {
			dump = append(dump, resultListEntry{key, packg.Caption()})
		})
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application-json")
		json.NewEncoder(w).Encode(map[string]interface{}{
			"version": "1",
			"name":    registry.Caption(),
			"result":  dump,
		})
	})
}


// Creates the full HTTP ServeMux to be used in any HTTP Server
// (an HTTP server has to be manually created with the mux returned
// by this function).
func CreateHTTPServeMux(registry *resources.Registry) *http.ServeMux {
	mux := http.NewServeMux()
	mux.Handle("/", RegistryAllHandler(registry))
	registry.EnumeratePackages(func(packageKey string, packg *resources.Package) {
		mux.Handle("/" +packageKey, PackageAllHandler(packg))
		packg.EnumerateSpaces(func(spaceKey string, space *resources.Space) {
			mux.Handle("/" + packageKey + "/" + spaceKey, SpaceAllHandler(space))
			space.EnumerateLists(func(listKey string, list *resources.List) {
				url := "/" + packageKey + "/" + spaceKey + "/" + listKey
				mux.Handle(url, ListAllHandler(list))
				mux.Handle(url + "/one", ListOneHandler(list))
				mux.Handle(url + "/many", ListManyHandler(list))
			})
		})
	})
	return mux
}