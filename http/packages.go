package http

import (
	"net/http"
	"encoding/json"
	"gitlab.com/gamemeanmachine/resource-servers"
)


// Returns an HTTP handler to just json-print the registered spaces.
// This handler can be directly used by a standard HTTP server.
func PackageAllHandler(packg *resources.Package) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump := []resultListEntry{}
		packg.EnumerateSpaces(func(key string, space *resources.Space) {
			dump = append(dump, resultListEntry{key, space.Caption()})
		})
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application-json")
		json.NewEncoder(w).Encode(map[string]interface{}{
			"version": "1",
			"name": packg.Caption(),
			"result": dump,
		})
	})
}
