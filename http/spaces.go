package http

import (
	"gitlab.com/gamemeanmachine/resource-servers"
	"net/http"
	"encoding/json"
)


// Returns an HTTP handler to just json-print the registered lists.
// This handler can be directly used by a standard HTTP server.
func SpaceAllHandler(space *resources.Space) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump := []resultListEntry{}
		space.EnumerateLists(func(key string, list *resources.List) {
			dump = append(dump, resultListEntry{key, list.Caption()})
		})
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application-json")
		json.NewEncoder(w).Encode(map[string]interface{}{
			"version": "1",
			"name": space.Caption(),
			"result": dump,
		})
	})
}