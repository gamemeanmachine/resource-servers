package http

import (
	"net/http"
	"encoding/json"
	"strconv"
	"strings"
	"fmt"
	"gitlab.com/gamemeanmachine/resource-servers"
	"reflect"
)


// Returns an HTTP handler to just json-print the involved resources.
// This handler can be directly used by a standard HTTP server.
func ListAllHandler(list *resources.List) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump := map[uint64]interface{}{}
		list.EnumerateObjects(func(id uint64, object resources.Object) {
			if object == nil || reflect.ValueOf(object).IsNil() {
				dump[id] = nil
			} else {
				dump[id] = object.Content()
			}
		})
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application-json")
		json.NewEncoder(w).Encode(map[string]interface{}{
			"version": "1",
			"name": list.Caption(),
			"result": dump,
		})
	})
}


// Returns an HTTP handler to just json-print the involved resources
// but for the given comma-separated list of ids.
func ListManyHandler(list *resources.List) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		stringIds := strings.Split(r.URL.Query().Get("ids"), ",")
		if len(stringIds) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			w.Header().Add("Content-Type", "application-json")
			json.NewEncoder(w).Encode(map[string]interface{}{
				"version": "1",
				"name": list.Caption(),
				"error": map[string]string{
					"type": "query-string-params",
					"what": "ids",
					"code": "empty",
					"description": "It must not be empty",
				},
			})
		} else {
			intIds := make([]uint64, len(stringIds))
			for index, stringId := range stringIds {
				if id, err := strconv.ParseUint(stringId, 10, 64); err != nil {
					w.WriteHeader(http.StatusBadRequest)
					w.Header().Add("Content-Type", "application-json")
					json.NewEncoder(w).Encode(map[string]interface{}{
						"version": "1",
						"name": list.Caption(),
						"error": map[string]string{
							"type": "query-string-params",
							"what": fmt.Sprintf("ids[%d]", index),
							"code": "empty",
							"description": "It must not be empty",
						},
					})
					return
				} else {
					intIds[index] = id
				}
			}
			dump := map[uint64]interface{}{}
			list.EnumerateOnly(intIds, func(id uint64, object resources.Object) {
				if object == nil || reflect.ValueOf(object).IsNil() {
					dump[id] = nil
				} else {
					dump[id] = object.Content()
				}
			})
			w.WriteHeader(http.StatusOK)
			w.Header().Add("Content-Type", "application-json")
			json.NewEncoder(w).Encode(map[string]interface{}{
				"version": "1",
				"name": list.Caption(),
				"result": dump,
			})
		}
	})
}


// Returns an HTTP handler to just json-print the involved resource
// but for the given id.
func ListOneHandler(list *resources.List) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if id, err := strconv.ParseUint(r.URL.Query().Get("id"), 10, 64); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Header().Add("Content-Type", "application-json")
			json.NewEncoder(w).Encode(map[string]interface{}{
				"version": "1",
				"name": list.Caption(),
				"error": map[string]string{
					"type": "query-string-params",
					"what": "id",
					"code": "invalid",
					"description": "Expected an unsigned integer",
				},
			})
		} else {
			w.WriteHeader(http.StatusOK)
			w.Header().Add("Content-Type", "application-json")
			object := list.Get(id)
			content := (interface{})(nil)
			if object != nil {
				content = object.Content()
			}
			json.NewEncoder(w).Encode(map[string]interface{}{
				"version": "1",
				"name": list.Caption(),
				"result": content,
			})
		}
	})
}