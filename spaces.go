package resources

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"fmt"
)


// A space is some sort of list of lists.
// One can iterate over all the available
// resources list, and also get one specific
// resource list by its key.
type Space struct {
	key        string
	caption    string
	listsMap   map[string]*List
	listsArray []*List
}


// Gets the space's key.
func (space *Space) Key() string {
	return space.key
}


// Gets the space's caption.
func (space *Space) Caption() string {
	return space.caption
}


// Enumerates all the resources lists registered
// in this space, in the order they were added.
func (space *Space) EnumerateLists(callback func(key string, list *List)) {
	for _, list := range space.listsArray {
		callback(list.Key(), list)
	}
}


// Gets a registered resources list by its key.
func (space *Space) Get(key string) *List {
	return space.listsMap[key]
}


// Adds a new list to this space, if the key is
// not already being used.
func (space *Space) Register(list *List) {
	if list == nil {
		panic(errors.Argument(
			"resources.Space::Register", "list",
			"Must not be nil",
		))
	} else if _, ok := space.listsMap[list.Key()]; ok {
		panic(errors.Argument(
			"resources.Space::Register", "list",
			fmt.Sprintf("A list with the same key already exists: %s", list.Key()),
		))
	} else {
		space.listsMap[list.Key()] = list
		space.listsArray = append(space.listsArray, list)
	}
}


// Creates a new resources space.
func NewSpace(key, caption string) *Space {
	if !keyPattern.MatchString(key) {
		panic(errors.Argument(
			"resources.NewSpace", "key",
			"Must consist of lowercase letters, numbers, and single-hyphens",
		))
	}
	return &Space{key, caption,map[string]*List{}, []*List{}}
}