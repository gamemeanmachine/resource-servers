package resources

import (
	"fmt"
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// This error will be raised when no a dependency
// is missing in a package.
type DependencyError struct {
	errors.GMMError
	depKey string
}


// Returns the failing/missing dependency.
func (dependencyError DependencyError) DependencyKey() string {
	return dependencyError.depKey
}


// Returns the full message error in compliance to the
// error interface.
func (dependencyError DependencyError) Error() string {
	return fmt.Sprintf(
		"Dependency error in registry for package key %s: Either no package is registered or a different " +
		"one was added under the same key",
		dependencyError.depKey,
	)
}


// Creates a dependency error to be triggered.
func Dependency(depKey string) DependencyError {
	return DependencyError{
		errors.GMM(
			"dependency",
			"Missing Dependency",
		),
		depKey,
	}
}


// Registries are the main point of a resources
// server. They register the packages, and the flow
// goes all down from that point when downloading
// the resources data.
type Registry struct {
	packagesMap   map[string]*Package
	packagesArray []*Package
	caption       string
}


// Gets the registry's caption.
func (registry *Registry) Caption() string {
	return registry.caption
}


// Enumerates all the resources packages registered
// in this registry, in the order they were added.
func (registry *Registry) EnumeratePackages(callback func(key string, packg *Package)) {
	for _, packg := range registry.packagesArray {
		callback(packg.Key(), packg)
	}
}


// Gets a registered resources package by its key.
func (registry *Registry) Get(key string) *Package {
	return registry.packagesMap[key]
}


// Adds a new list to this space, if the key is
// not already being used.
func (registry *Registry) Register(packg *Package) {
	if packg == nil {
		panic(errors.Argument(
			"resources.Registry::Register", "package",
			"Must not be nil",
		))
	} else if _, ok := registry.packagesMap[packg.Key()]; ok {
		panic(errors.Argument(
			"resources.Registry::Register", "package",
			fmt.Sprintf("A package with the same key already exists: %s", packg.Key()),
		))
	} else {
		packg.EnumerateDependencies(func(key string, dep *Package) {
			if dep2, _ := registry.packagesMap[key]; dep2 != dep {
				panic(Dependency(key))
			}
		})
		registry.packagesMap[packg.Key()] = packg
		registry.packagesArray = append(registry.packagesArray, packg)
	}
}


// Creates a new, empty, registry.
func NewRegistry(caption string) *Registry {
	return &Registry{map[string]*Package{}, []*Package{}, caption}
}