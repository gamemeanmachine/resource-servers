package resources

import (
	"fmt"
	"reflect"
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// A list of resource objects will just register and get
// or enumerate the resources are they are needed
type List struct {
	key      string
	caption  string
	elemType reflect.Type
	entries  map[uint64]Object
}


// Registering a resource object into this list, if the
// object satisfies the required type and its ID is not
// yet used. Otherwise, it panics.
func (list *List) Register(object Object) {
	if object == nil {
		panic(errors.Argument(
			"resources.List::Register", "object",
			"Must not be nil",
		))
	} else if !reflect.TypeOf(object).AssignableTo(list.elemType) {
		panic(errors.Argument(
			"resources.List::Register", "object",
			"Must be of a type assignable to the list's type",
		))
	} else if _, ok := list.entries[object.ID()]; ok {
		panic(errors.Argument(
			"resources.List::Register", "object",
			fmt.Sprintf("Must have a unique ID. The ID %d is already in use", object.ID()),
		))
	} else {
		list.entries[object.ID()] = object
	}
}


// Returns the list's key.
func (list *List) Key() string {
	return list.key
}


// Returns the list's caption.
func (list *List) Caption() string {
	return list.caption
}


// Dumps all the resources contents by iteration.
func (list *List) EnumerateObjects(callback func(id uint64, object Object)) {
	for id, object := range list.entries {
		callback(id, object)
	}
}


// Dumps many resources by iteration. It may include nil entries.
func (list *List) EnumerateOnly(ids []uint64, callback func(id uint64, object Object)) {
	for _, id := range ids {
		callback(id, list.entries[id])
	}
}


// Gets a specific resource. It may return nil.
func (list *List) Get(id uint64) Object {
	return list.entries[id]
}


// Creates a new resources list, by giving its template type
// (which in turn must implement the `Object` interface).
func NewList(key, caption string, template Object) *List {
	templateType := reflect.TypeOf(template)
	if !keyPattern.MatchString(key) {
		panic(errors.Argument(
			"resources.NewList", "key",
			"Must consist of lowercase letters, numbers, and single-hyphens",
		))
	}
	return &List{key, caption, templateType, map[uint64]Object{}}
}