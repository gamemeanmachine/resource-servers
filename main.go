package resources

import (
	"regexp"
)


// Resource objects implement this interface to dump
// their contents. The result is arbitrary, and it is
// a matter of the json package to dump it appropriately.
type Object interface {
	ID()      uint64
	Content() interface{}
}


var keyPattern, _ = regexp.Compile("[a-z0-9_]+(-[a-z0-9_]+)*")



